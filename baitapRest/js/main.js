let tinhDTB = (...diemRest) => {
  let tong = 0;
  let soMon = diemRest.length;
  let tinhTong = (diem) => {
    tong += parseFloat(diem);
  };
  diemRest.map(tinhTong);
  console.log(tong);
  return (tong / soMon).toFixed(2);
};

document.getElementById("btnKhoi1").onclick = () => {
  let toan = document.getElementById("inpToan").value;
  let ly = document.getElementById("inpLy").value;
  let hoa = document.getElementById("inpHoa").value;
  document.getElementById("tbKhoi1").innerHTML = tinhDTB(toan, ly, hoa);
};

document.getElementById("btnKhoi2").onclick = () => {
  let van = document.getElementById("inpVan").value,
    su = document.getElementById("inpSu").value,
    dia = document.getElementById("inpDia").value,
    anh = document.getElementById("inpEnglish").value;
  document.getElementById("tbKhoi2").innerHTML = tinhDTB(van, su, dia, anh);
};
