const colorList = [
  "pallet",
  "viridian",
  "pewter",
  "cerulean",
  "vermillion",
  "lavender",
  "celadon",
  "saffron",
  "fuschia",
  "cinnabar",
];
let container = document.getElementById("colorContainer");
let loadColorPick = () => {
  for (let index = 0; index < colorList.length; index++)
    if (index == 0) {
      container.innerHTML +=
        "<button class='color-button " +
        colorList[index] +
        " active'></button>";
    } else {
      container.innerHTML +=
        "<button class='color-button " + colorList[index] + "'></button>";
    }
};

loadColorPick();
let colorPicker = document.getElementsByClassName("color-button");
let house = document.getElementById("house");

for (let index = 0; index < colorPicker.length; index++) {
  colorPicker[index].addEventListener("click", function () {
    changeColor(colorList[index], index);
  });
}

let changeColor = (index, newIndex) => {
  for (let index = 0; index < colorPicker.length; index++)
    colorPicker[index].classList.remove("active");
  colorPicker[newIndex].classList.add("active"),
    (house.className = "house " + index);
};
